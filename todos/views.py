from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# list view
def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos
    }
    return render(request, "todos/list.html", context)


# detail view
def todo_list_detail(request, id):
    detail = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": detail
    }
    return render(request, "todos/detail.html", context)


# create list
def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            detail = form.save()
            return redirect("todo_list_detail", id=detail.id)
    else:
        form = TodoListForm()
    context = {
        "form": form
    }
    return render(request, "todos/create.html", context)


# update list
def todo_list_update(request, id):
    detail = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=detail)
        if form.is_valid():
            detail = form.save()
            return redirect("todo_list_detail", id)
    else:
        form = TodoListForm(instance=detail)

    context = {
        "form": form
    }
    return render(request, "todos/edit.html", context)


# delete list
def todo_list_delete(request, id):
    detail = TodoList.objects.get(id=id)
    if request.method == "POST":
        detail.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")


# create item
def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            detail = form.save()
            return redirect("todo_list_detail", id=detail.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form
    }
    return render(request, "todos/create-item.html", context)


# update item
def todo_item_update(request, id):
    item = TodoItem.objects.get(id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            item = form.save()
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        "form": form
    }
    return render(request, "todos/edit-item.html", context)
